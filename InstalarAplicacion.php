<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Modelo.php");
        include_once("funciones.php");
        cabecera();

        $modelo = obtenerModelo();
        $modelo->instalar();

        pie();
        inicio();
        ?>
    </body>
</html>
