<?php include_once("funciones.php"); ?>
<?php include_once("Modelo.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <?php cabecera(); ?>

        <p>Gestión de Asignaturas:</p>
        <ul>
            <li><a href="AsignaturaFormulario.php">Alta </a> </li>
        </ul>

        <?php
        $modelo = obtenerModelo();
        $asignaturas = $modelo->getAsignaturas();

        //print_r($asignaturas);

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id Asignatura</td>';
        echo '<td>Nombre</td>';
        echo '<td>Horas</td>';
        echo '<td>Id Profesor</td>';
        echo '</tr>';

        echo "<p>Listado:</p>";

        foreach ($asignaturas as $asignatura) {

            $profesor = new Profesor("", "");
            $profesor = $asignatura->getProfesor();

            //print_r($profesor);

            echo "<tr>";
            echo "<td>" . $asignatura->getId() . "</td>";
            echo "<td>" . $asignatura->getNombre() . "</td>";
            echo "<td>" . $asignatura->getHoras() . "</td>";
            echo "<td>" . $profesor->getId() . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo "<br/>";
        ?>

        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>
