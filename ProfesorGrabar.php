
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Modelo.php");
        include_once("funciones.php");

        cabecera();
        echo "<h2>Profesor Grabar</h2>";

        function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $profe = new Profesor($id, $nombre);
            return $profe;
        }

        //***************************
        //* Main
        //***************************

        $profesor = leer();

        if ($profesor->getNombre() == "") {
            echo "Error: Nombre vacio" . "<br>";
        } else {
            $modelo = obtenerModelo();
            $res = $modelo->grabarProfesor($profesor);
            if ($res) {
                echo "Grabado: " . $profesor->getNombre() . "<br>";
            } else {
                echo "Error: No grabado: " . $profesor->getNombre() . "<br>";
            }
        }

        inicio();
        pie();
        ?>
    </body>
</html>
