<?php include_once("Config.php"); ?>
<?php include_once("funciones.php"); ?>
<?php include_once("Modelo.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php Config::$titulo ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <?php cabecera(); ?>

        <p>Gestión de Profesor:</p>
        <ul>
            <li><a href="ProfesorFormulario.php">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id Prof</td>';
        echo '<td>Nom Prof</td>';
        echo '</tr>';

        $modelo = obtenerModelo();
        $profesores = $modelo->getProfesores();

        if (count($profesores) > 0) {

            foreach ($profesores as $profesor) {
                echo "<tr>";
                echo "<td>" . $profesor->getId() . "</td>";
                echo "<td>" . $profesor->getNombre() . "</td>";
                echo "</tr>";
            }
        }

        echo "</table>";
        echo "<br/>";
        ?>

        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>
