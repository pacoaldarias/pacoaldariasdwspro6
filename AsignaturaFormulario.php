<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Asignatura.php");
        include_once("Modelo.php");
        include_once("funciones.php");


        $modelo = obtenerModelo();
        $id = $modelo->calculaidmaxasignatura();
        ?>

        <h1>Datos del Asignatura</h1>
        <h1>Alta Asignatura</h1>
        <form method="POST" action="AsignaturaGrabar.php" >
            <table border="1">

                <tr>
                    <td>Id:</td>

                    <td>

                        <input type="hidden" name="id"
                        <?php
                        echo 'value="' . $id . '"> ';
                        echo $id;
                        ?>

                    </td>
                </tr>

                <tr>
                    <td>Nombre:</td><td><input type="text" name="nombre" /></td>
                </tr>
                <tr>
                    <td>Horas Lectivas:</td><td><input type="text" name="horas" /></td>
                </tr>

                <tr>
                    <td>Profesor</td><td>
                        <?php
                        //$modelo = obtenerModelo();
                        $profesores = array();
                        $profesores = $modelo->getProfesores();
                        echo '<select name="profesor">';
                        foreach ($profesores as $profesor) {
                            $id = $profesor->getId();
                            $nombre = $profesor->getNombre();
                            echo "<option value='" . $id . "'>" . $nombre . "</option>";
                        }
                        echo "</select>";
                        ?>
                </tr>

                <tr>
                    <td><input type="submit"  name= "Enviar" value="Enviar"></td>
                    <td><input type="reset"   name= "Borrar" value="Borrar"></td>
                </tr>
            </table>
        </form>
        <?php inicio(); ?>
<?php pie(); ?>

    </body>
</html>