<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Modelo.php");

/**
 * Description of ModeloMysql
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class ModeloMysql implements Modelo {

    protected $conexion;

    public function __construct($dbname, $dbuser, $dbpass, $dbhost) {


        $this->conexion = NULL;

        try {

            $bdconexion = new PDO('mysql:host=' . $dbhost . ';dbname='
              . $dbname . ';charset=utf8', $dbuser, $dbpass);

            $this->conexion = $bdconexion;
        } catch (PDOException $e) {

            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getAsignaturas() {


        $consulta = "SELECT a.id, a.nombre, horas, p.id as idp,  p.nombre as nomp "
          . "FROM asignatura as a , profesor as p  where a.idp=p.id;";
        //echo $consulta;
        $result = $this->conexion->query($consulta);
        $asignaturas = array();
        $cont = 0;

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //print_r($filas);
        foreach ($filas as $fila) {
            //echo $fila->id . "<br>";
            $profesor = new Profesor($fila->idp, $fila->nomp);
            $asignaturas [$cont] = new Asignatura($fila->id, $fila->nombre, $fila->horas, $profesor);
            $cont++;
        }

        $conexion = false;
        return $asignaturas;
    }

    public function getProfesor($profesor_) {

        $consulta = 'SELECT * FROM profesor where id=:id;';
        //$consulta = 'SELECT * FROM profesor where id="' . $profesor_->getId() . '";';
        //$result = $this->conexion->query($consulta);

        $result = $this->conexion->prepare($consulta);
        $result->execute(array(":id" => $profesor_->getId()));

        $profesores = array();
        $cont = 0;

        $fila = $result->fetch(PDO::FETCH_OBJ);
        $profesor = new Profesor($fila->id, $fila->nombre);

        $conexion = false;

        return $profesor;
    }

    public function getProfesores() {

        $consulta = "SELECT * FROM profesor";
        $result = $this->conexion->query($consulta);
        $profesores = array();
        $cont = 0;


        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //print_r($filas);
        foreach ($filas as $fila) {
            //echo $fila->id . "<br>";
            $profesores [$cont] = new Profesor($fila->id, $fila->nombre);
            $cont++;
        }

        $conexion = false;

        return $profesores;
    }

    public function calculaidmaxasignatura() {
        // Calcula el id max de profesor

        try {
            $consulta = 'select max(id) as max from asignatura;';
            $query = $this->conexion->query($consulta);
            $fila = $query->fetch();
            $id = $fila["max"];
            //print_r($fila);
            if (!$id) {
                $id = 0;
            }
            //echo $consulta . "<br>";
            //echo $id . "<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return 0;
        }
        return $id + 1;
    }

    public function grabarAsignatura($asignatura) {


        $consulta = 'INSERT INTO asignatura ( id, nombre, horas, idp ) '
          . 'VALUES( :id,  :nombre, :horas, :idp);';

        //echo $consulta . "<br\>";
        //print_r($asignatura);
        //echo "<br\>";
        //echo "Grabando: " . $asignatura->getId() . $asignatura->getNombre() . $asignatura->getHoras() . $asignatura->getProfesor()->getId();

        $result = $this->conexion->prepare($consulta);

        if ($result) {
            echo "<br>";
            echo "\nPDO::errorInfo():<br>\n";
            print_r($result);
            print_r($this->conexion);
        }


        /* Ejecuta una sentencia preparada pasando un array de valores */
        $count = $result->execute(array(
          ":id" => $asignatura->getId()
          , ":nombre" => $asignatura->getNombre()
          , ":horas" => $asignatura->getHoras()
          , ":idp" => $asignatura->getProfesor()->getId()));


        echo "Count: " . $count . "<br>";
        print_r($result->errorInfo());

        $this->conexion = false;

        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    // Calcula el id max de profesor
    public function calculaidmaxprofesor() {

        // Calcula el id max de profesor

        try {
            $consulta = 'select max(id) as max from profesor;';
            $query = $this->conexion->query($consulta);
            $fila = $query->fetch();
            $id = $fila["max"];

            if (!$id) {
                $id = 0;
            }
            //print_r($fila);
            //echo $consulta . "<br>";
            //echo $id . "<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return 0;
        }
        return $id + 1;
    }

    public function grabarProfesor($profesor) {


        try {
            $consulta = 'INSERT INTO profesor (id, nombre) VALUES (:id, :nombre) ;';
            $result = $this->conexion->prepare($consulta);

            if (!$result) {
                //echo "\nPDO::errorInfo():<br>";
                //print_r($this->conexion->errorInfo());
            }

            $count = $result->execute(array(
              ":id" => $profesor->getId()
              , ":nombre" => $profesor->getNombre()));

            //echo $consulta . "<br\>";
            //echo "Count: " . $count . "<br\>";
            //print_r($result);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return false;
        }

        $conexion = false;
        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function instalar() {

        echo "<h2>Instalando: " . Config::$modelo . "</h2>";
        try {
            // Conectamos sin indicar bbdd
            $conexion = new PDO("mysql:host=" . Config::$bdhostname . ".", Config::$bdusuario, Config::$bdclave);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }


        //creamos la base de datos si no existe
        try {
            $crear_bd = $conexion->prepare('CREATE DATABASE IF NOT EXISTS ' . Config::$bdnombre . ' COLLATE utf8_spanish_ci');
            $crear_bd->execute();
            echo "Creada BDDD: " . Config::$bdnombre . "<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }


        //decimos que queremos usar la BBDD que acabamos de crear
        try {
            $use_db = $conexion->prepare('USE ' . Config::$bdnombre);
            $use_db->execute();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }


        try {
            //creamos la tabla profesor
            $sql = 'CREATE TABLE IF NOT EXISTS profesor (
            id int(3) NOT NULL,
            nombre varchar(80) COLLATE utf8_spanish_ci,
            PRIMARY KEY (id));';

            //echo $sql . "<br>";

            $crear_tb_profesor = $conexion->prepare($sql);
            $crear_tb_profesor->execute();
            echo "Creada tabla: profesor<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = 'CREATE TABLE IF NOT EXISTS asignatura (
                id int(11) NOT NULL ,
		nombre varchar(100) COLLATE utf8_spanish_ci NOT NULL,
                horas int(3) NOT NULL,
                idp int(11) NOT NULL,
		PRIMARY KEY (id)
                );';
            //echo $sql . "<br>";
            $crear_tb_asignatura = $conexion->prepare($sql);
            $crear_tb_asignatura->execute();
            echo "Creada tabla: asignatura<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = "ALTER TABLE asignatura ADD CONSTRAINT fk_user_id FOREIGN KEY (idp) references profesor(id);";
            $fk = $conexion->prepare($sql);
            $fk->execute();
            echo "Creada CAj: Asignatura.idp -> Profesor.id <br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        $conexion = false;
    }

}
