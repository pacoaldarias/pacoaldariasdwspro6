<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Modelo.php");
        include_once("funciones.php");

        //**************************************
        function leerAsignatura() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $horas = recoge("horas");
            $profesorid = recoge("profesor");
            //echo "Leido form:  Asig:" . $nombre . " Profesor:" . $profesorid . "<br>";
            $profesor_ = new Profesor($profesorid, "");
            $modelo = obtenerModelo();
            $profesor = $modelo->getProfesor($profesor_);
            $asignatura = new Asignatura($id, $nombre, $horas, $profesor);

            return $asignatura;
        }

        //*****************************
        //*  main
        //*****************************

        $asignatura = leerAsignatura();

        /*
          echo "Leido Asign: " . $asignatura->getId()
          . " " . $asignatura->getNombre()
          . " " . $asignatura->getProfesor()->getId()
          . "<br>";
         */

        if ($asignatura->getNombre() == "") {
            echo "Error: Nombre asignatura vacio" . "<br>";
        } else if ($asignatura->getHoras() == "") {
            echo "Error: Horas vacio" . "<br>";
        } else if ($asignatura->getProfesor()->getId() == "") {
            echo "Error: Nombre profesor vacio" . "<br>";
        } else {
            $modelo = obtenerModelo();
            if ($modelo->grabarAsignatura($asignatura)) {
                echo "Grabado: " . $asignatura->getNombre() . "<br>";
            } else {
                echo "No grabado: " . $asignatura->getNombre() . "<br>";
            }
        }

        echo "<a href='index.php'>Volver</a>";
        ?>
    </body>
</html>
